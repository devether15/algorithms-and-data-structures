//my solution
function averagePair(arr,target){
    const tgt = target;
    let left = 0;

    let right = arr.length -1;
    while(left < right){
        let avg = arr[left] + arr[right] / 2;
        if (avg === tgt){
            return true;
        }else{
            right--;
            left++;
        }
    }
    return false;
}

//mentor solution
/* function averagePair(arr, num){
    let start = 0
    let end = arr.length-1;
    while(start < end){
      let avg = (arr[start]+arr[end]) / 2 
      if(avg === num) return true;
      else if(avg < num) start++
      else end--
    }
    return false;
  } */

console.log(averagePair([1,2,3],2.5)); //true
console.log(averagePair([1,3,3,5,6,7,12,19],8)); //true
console.log(averagePair([-1,0,3,4,5,6],4.1)); //false
console.log(averagePair([],4)); //false
