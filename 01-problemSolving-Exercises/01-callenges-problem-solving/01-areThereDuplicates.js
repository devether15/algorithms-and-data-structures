//implement a function called, areThereDuplicated which accepts a variable number of arguments,
//and checks whether there are any duplicates among the arguments passed in. You can solve this
//using the requency counter pattern OR the multiple pointers pattern.

//my solution
function areThereDuplicates(...args) {
    let myArray = [...args]

    const frequencyCounter = {};
    
    for (let i=0; i < myArray.length; i++) {
        let number = myArray[i];
        frequencyCounter[number] ? frequencyCounter[number] += 1 : frequencyCounter[number] = 1;

        if(frequencyCounter[number] > 1) return true
    }

    return false;
}

//profesor solution
/* function areThereDuplicates() {
    console.log(arguments)
    let collection = {}
    for(let val in arguments){
      collection[arguments[val]] = (collection[arguments[val]] || 0) + 1
    }
    for(let key in collection){
      if(collection[key] > 1) return true
    }
    return false;
  } */

console.log(areThereDuplicates(1,2,3)) //false
console.log(areThereDuplicates(1,2,2)) //true
console.log(areThereDuplicates(1,2,2)) //true