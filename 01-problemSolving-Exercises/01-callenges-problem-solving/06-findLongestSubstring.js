// my attempt (lol it sucks)
// function findLongestSubstring(string){
//     let start = 0;
//     let end =0;   
//     let maxLen = -Infinity;

//     let frequencyCounter = {};

//     while ( start < string.length) {

//       if(end < string.length){       
//           frequencyCounter[string[end]] ? frequencyCounter[string[end]] += 1 : frequencyCounter[string[end]] = 1;
//           end++
//           console.log(frequencyCounter);
//         }

//         // else if(total >= sum){
//         //   maxLen = Math.max(maxLen, end-start);
//         //   start++;
//         // } 
//         // // current total less than required total but we reach the end, need this or else we'll be in an infinite loop 
//         // else {
//         //   break;
//         // }
//     }

//     // return maxLen;

//   }

function findLongestSubstring(str) {
  let longest = 0;
  let seen = {};
  let start = 0;
 
  for (let i = 0; i < str.length; i++) {
    let char = str[i];
    if (seen[char]) {
      start = Math.max(start, seen[char]);
    }
    // index - beginning of substring + 1 (to include current in count)
    longest = Math.max(longest, i - start + 1);
    // store the index of the next char so as to not double count
    seen[char] = i + 1;
  }
  return longest;
}

//   console.log(findLongestSubstring('')) //0
//   console.log(findLongestSubstring('rithmschool')) //7
//   console.log(findLongestSubstring('thisisawesome')) //6
//   console.log(findLongestSubstring('thecatinthehat'))//7
//   console.log(findLongestSubstring('bbbbbbb')) //1
//   console.log(findLongestSubstring('longestsubstring')) //8
  console.log(findLongestSubstring('thisishowwedoit'))//6
  