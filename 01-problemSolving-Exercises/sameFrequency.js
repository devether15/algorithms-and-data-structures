function sameFrequency(first,second){

  let arr1 = first.toString();
  let arr2 = second.toString();

  if(arr1.length !== arr2.length){
      return false;
  }
  
  const frequencyCounter = {};
  
  for(let i = 0; i < arr1.length; i++){
      let number1 = arr1[i];
      frequencyCounter[number1] ? frequencyCounter[number1] += 1 : frequencyCounter[number1] = 1;
  }
  
  console.log(frequencyCounter);
  
  for(let i = 0; i < arr2.length; i++){
    let number2 = arr2[i];
    if (!frequencyCounter[number2]) {
      return false;
    } else {
      frequencyCounter[number2] -= 1;
    }
  }

  console.log(frequencyCounter);

  return true;
}

sameFrequency(182,281);